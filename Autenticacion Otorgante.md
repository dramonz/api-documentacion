﻿
# Autenticación Otorgante

## Descripción

Asegurar quien que va a recibir el credito es quien dice ser, mediante un conjunto de preguntas y respuestas proporciandas por el otorgante.

El servicio cuando con dos etapas una de generación de preguntas y otro la evaluación de estas.

## Generar Preguntas

La generación de preguntas es el primer paso para la autenticacíon de la persona.

### URL
> http://localhost:8080/mx/v1/autenticacion/otorgante

### HTTP Method: POST

#### Request Parameters

|Propiedad|Tipo|¿Requerido?|Descripción|
|-------|-------|-------|-------|
|primerNombre|String|Si|El **primer nombre** completo del cliente, No usar abreviaturas, iniciales, acentos y/o puntos.
|segundoNombre|String|No|El **segundo nombre** completo del cliente, No usar abreviaturas, iniciales, acentos y/o puntos.
|apellPaterno|String|Si|El **apellido paterno** completo del Cliente. No usar abreviaturas, iniciales y/o puntos.
|apellMaterno|String|No|El **apellido materno** completo del Cliente. No usar abreviaturas, iniciales y/o puntos.
|apellAdicional|String|No|Se debe utilizar para reportar el **apellido de casada**.
|lsFecha|String|Si|**Fecha de nacimiento**
|rfc|String|No|Reglas:<br>Las primeras 4 posiciones deben ser alfabéticas.<br>1. 5 y 6 deben contener un número entre 00 y 99.<br>2. 7 y 8 deben contener un número entre 01 a 12.<br>3. 9 y 10 deben contener un número entre 01 a 31.  <br>4. 11 -13 homoclave (opcional).<br> Los RFC's de personas extranjeras deben cumplir con las características arriba mencionadas|
|direccion|String|Si|La **dirección de la persona** incluyendo nombre de la calle, número exterior y/o interior. Deben considerarse avenida, cerrada, manzana, lote, edificio, departamento etc.
|colonia|String|No|La **colonia** a la cual pertenece la dirección de la persona
|ciudad|String|No|La **Ciudad** a la cual pertenece la dirección de la persona
|codigo|String|Si|El **código postal** reportado debe estar compuesto por 5 dígitos; para que este sea válido deberá corresponder al rango que se maneja en dicho Estado.  <br>En caso de una longitud de 4 dígitos completar con apóstrofe y cero a la izquierda ('08564)
|delegMunic|String|No|La **delegación** a la pertenece la dirección de la persona
|estado|String|Si|La abreviatura correspondiente
|pais|String|Si|Debe contener el país donde se encuentra el domicilio del Acreditado
|curp|String|No|Reglas: <br>  1- 4 posiciones deben ser alfabéticas.  <br>2- 5 y 6 posiciones deben contener un número entre 00 y 99 (año).<br>3- 7 y 8 posiciones deben contener un número entre 01 y 12 (mes).<br>4- 9 y 10 posiciones deben contener un número entre 01 y 31 (día).  <br>5- 11-16 posiciones deben ser alfabéticas.  <br>6- 17 numérico (homoclave).  <br>7- 18 numérico (Dígito Verificador).
|folio|String|Si|
|usuario|String|Si|Usuario que consulta el servicio

#### Ejemplo Request - JSON

```bash
curl -H "Content-Type: application/json" -X POST -d '{"primerNombre":"JUAN","segundoNombre":"","apellPaterno":"PRUEBA","apellMaterno":"OCHO","apellAdicional":"","lsFecha":"1980-01-08","direccion":"INSURGENTES SUR 1008","colonia":"INSURGENTES","ciudad":"BENITO JUAREZ","codigo":"04470","delegMunic":"INSURGENTES","estado":"DF","rfc":"COPJ800108","pais":"MEX","curp": "SASSASSASSA","folio":"API123","usuario":"CDCAPI0001"}' http://localhost:8080/mx/v1/autenticacion/otorgante
```

#### Response Properties
|Propiedad|Tipo|Descripción|
|-------|-------|-------|
|cvePersona|int|**Clave de la persona**
|numSolicitud|int|**Número de Solicitud**
|numAutenticacion|String|**Número de autenticación**
|intentos|int|Intentos realizados el maximo de intentos son 3
|opcionesTC|Objeto|Opciones de pregunta para Tarjeta de credito
|otorganteTC|Objeto Otorgante|Otorgante de Tarjeta de Credito
|└─cveOtorgante|String|**Clave del otorgante**
|└─nombOtorgante|String|**Nombre corto del otorgante**
|└─tipoNegocioOtor|String|T=Tarjeta de credito<br>A=Credito Automotriz<br>H=Credito Hipotecario
|cuatroDigitosTC|String|Cuatro digitos de la Tarjeta de Credito
|creditoHipotecario|boolean|Si tiene Credito Hipotecario
|otorganteHB|Objeto Otorgante|Otorgante de Credito Hipotecario
|└─cveOtorgante|String|**Clave del otorgante**
|└─nombOtorgante|String|**Nombre corto del otorgante**
|└─tipoNegocioOtor|String|T=Tarjeta de credito<br>A=Credito Automotriz<br>H=Credito Hipotecario
|opcionesCuandoCreoHB|Objeto|Opciones de Cuando Creo el Credito Hipotecario
|opcionesConQuienTieneHB|Arreglo de Otorgante|Opciones de Otorgantes con quien tiene Credito hipotecario
|└─cveOtorgante|String|Clave del otorgante
|└─nombOtorgante|String|Nombre corto del otorgante
|└─tipoNegocioOtor|String|T=Tarjeta de credito<br>A=Credito Automotriz<br>H=Credito Hipotecario
|opcionesCuandoCreoAB|Objeto|Opciones de cuando creo el credito Automotriz
|otorganteAB|Objeto Otorgante|otorgante de credito automotriz
|└─cveOtorgante|String|**Clave del otorgante**
|└─nombOtorgante|String|**Nombre corto del otorgante**
|└─tipoNegocioOtor|String|T=Tarjeta de credito<br>A=Credito Automotriz<br>H=Credito Hipotecario
|opcionesCierreAB|Objeto|Opciones de terminacion del credito automotriz
|opcionesConQuienTieneAB|Arreglo de Otorgante|Opciones de Otorgantes con quien tiene credito automotriz
|└─cveOtorgante|String|Clave del otorgante
|└─nombOtorgante|String|Nombre corto del otorgante
|└─tipoNegocioOtor|String|T=Tarjeta de credito<br>A=Credito Automotriz<br>H=Credito Hipotecario

#### Ejemplo Response - JSON

```bash
{
	"response":{
		"isSuccess":true,
		"success":{
			"title":"Petición exitosa",
			"message":"Se conecto de forma correcta"
		},
		"errors":[],
		"data":{
			"cvePersona":40212897,
			"numSolicitud":"18582430",
			"opcionesTC":{
				"a":"Entre 1-3",
				"b":"Entre 3-5",
				"c":"No tiene"
			},
			"otorganteTC":{
				"cveOtorgante":"000881",
				"nombOtorgante":"SANBORNS",
				"tipoNegocioOtor":"T"
			},
			"cuatroDigitosTC":"0000",
			"creditoHipotecario":false,
			"otorganteHB":{
				"cveOtorgante":"000391",
				"nombOtorgante":"INVIDF",
				"tipoNegocioOtor":"H"
			},
			"opcionesCuandoCreoHB":{
			"a":"1-3 años",
			"b":"más de 3 años",
			"c":"No tengo ese crédito"
			},
			"opcionesConQuienTieneHB":[
				{
					"cveOtorgante":null,
					"nombOtorgante":"Ninguno",
					"tipoNegocioOtor":"H"
				},
				{
					"cveOtorgante":"000391",
					"nombOtorgante":"INVIDF",
					"tipoNegocioOtor":"H"
				},
				{
					"cveOtorgante":"000642",
					"nombOtorgante":"URBI",
					"tipoNegocioOtor":"H"
				},
				{
					"cveOtorgante":"000107",
					"nombOtorgante":"AFIRME",
					"tipoNegocioOtor":"H"
				}
			],
			"opcionesCuandoCreoAB":{
				"a":"menos de 1 año",
				"b":"entre 1 y 3 años",
				"c":"más de tres años",
				"d":"no tengo"
			},
			"otorganteAB":{
				"cveOtorgante":"000534",
				"nombOtorgante":"INTEGRA",
				"tipoNegocioOtor":"A"
			},
			"opcionesCierreAB":{
				"a":"menos de 1 año",
				"b":"entre 1 y 3 años",
				"c":"más de tres años",
				"d":"no tuve ese crédito"
			},
			"opcionesConQuienTieneAB":[
				{
					"cveOtorgante":null,
					"nombOtorgante":"Ninguno",
					"tipoNegocioOtor":"A"
				},
				{
					"cveOtorgante":"000534",
					"nombOtorgante":"INTEGRA",
					"tipoNegocioOtor":"A"
				},
				{
					"cveOtorgante":"000728",
					"nombOtorgante":"LA ESPERANZA",
					"tipoNegocioOtor":"A"
				},
				{
					"cveOtorgante":"000457",
					"nombOtorgante":"SUAUTO",
					"tipoNegocioOtor":"A"
				}
			],
			"numAutenticacion":"97158"
		}
	}
}	
```


## Evaluar Preguntas

La evaluacion de preguntas es el segundo paso para la autenticación de la persona.

### URL
> http://localhost:8080/mx/v1/autenticacion/otorgante/{numeroAutenticacion}

### HTTP Method: PUT

#### Request Parameters

|Propiedad|Tipo|¿Requerido?|Descripción|
|-------|-------|-------|-------|
|cvePersona|int|Si|**Clave de la persona**
|numAutenticacion|String|Si|**Número de Autenticación**
|cuantasTCtiene|String|Si|Almacena el indice de la respuesta "**a,b,c, o d**"
|otorganteTC|Objeto Otorgante|Si|Otorgante de Tarjeta de Credito
|└─cveOtorgante|String|No|**Clave del otorgante**
|└─nombOtorgante|String|No|**Nombre corto del otorgante**
|└─tipoNegocioOtor|String|No|T=Tarjeta de credito<br>A=Credito Automotriz<br>H=Credito Hipotecario
|cuatroDigitosTC|String|No|Cuatro digitos de la Tarjeta de Credito
|creditoHipotecario|boolean|Si|Si tiene Credito Hipotecario
|otorganteHB|Objeto Otorgante|Si|Otorgante de Credito Hipotecario
|└─cveOtorgante|String|No|**Clave del otorgante**
|└─nombOtorgante|String|No|**Nombre corto del otorgante**
|└─tipoNegocioOtor|String|No|T=Tarjeta de credito<br>A=Credito Automotriz<br>H=Credito Hipotecario
|cuandoCreoHB|String|Si|Almacena el indice de la respuesta "**a,b,c, o d**"
|conQuienTieneHB|Objeto Otorgante|Si|Opciones de Otorgantes con quien tiene Credito hipotecario
|└─cveOtorgante|String|No|**Clave del otorgante**
|└─nombOtorgante|String|No|**Nombre corto del otorgante**
|└─tipoNegocioOtor|String|No|T=Tarjeta de credito<br>A=Credito Automotriz<br>H=Credito Hipotecario
|cuandoCreoAB|String|Si|Almacena el indice de la respuesta "**a,b,c, o d**"
|otorganteAB|Objeto Otorgante|Si|otorgante de credito automotriz
|└─cveOtorgante|String|No|**Clave del otorgante**
|└─nombOtorgante|String|No|**Nombre corto del otorgante**
|└─tipoNegocioOtor|String|No|T=Tarjeta de credito<br>A=Credito Automotriz<br>H=Credito Hipotecario
|cuandoTerminaAB|String|Si|Almacena el indice de la respuesta "**a,b,c, o d**"
|conquienTieneAB|Objeto Otorgante|Si|Con quien tiene credito automotriz
|└─cveOtorgante|String|No|**Clave del otorgante**
|└─nombOtorgante|String|No|**Nombre corto del otorgante**
|└─tipoNegocioOtor|String|No|T=Tarjeta de credito<br>A=Credito Automotriz<br>H=Credito Hipotecario

#### Ejemplo Request - JSON

```bash
curl -H "Content-Type: application/json" -X POST -d '{"cvePersona":40212897,"cuantasTCtiene":"a","otorganteTC":{"cveOtorgante":"000268","nombOtorgante":"BANCO AZTECA / CIRC. CRED MICRO FINANZAS","tipoNegocioOtor":"T"},"cuatroDigitosTC":"3561","creditoHipotecario":false,"otorganteHB":{"cveOtorgante":"000063","nombOtorgante":"FINPATRIA","tipoNegocioOtor":"H"},"cuandoCreoHB":"c","conquienTieneHB":{"cveOtorgante":"000063","nombOtorgante":"FINPATRIA","tipoNegocioOtor":"H"},"cuandoCreoAB":"d","otorganteAB":{"cveOtorgante":"000750","nombOtorgante":"FRACHEFI","tipoNegocioOtor":"A"},"cuandoTerminaAB":"d","conquienTieneAB":{"cveOtorgante":"000750","nombOtorgante":"FRACHEFI","tipoNegocioOtor":"A"},"numAutenticacion":"97156"}' http://localhost:8080/mx/v1/autenticacion/otorgante/97156
```

#### Response Properties
|Propiedad|Tipo|Descripción|
|-------|-------|-------|
|autenticacion|Boolean|Regresa verdadero si este autentica
|numAutenticacion|String|Número de autenticación
|intentos|int|Numero de intentos de la solicitud anteriores

#### Ejemplo Response - JSON

```bash
{
	"response":{
		"isSuccess":true,"success":{
			"title":"Petición exitosa",
			"message":"Se conecto de forma correcta"
		},
		"errors":[],
		"data":{
			"intentos":0,
			"numeroAutenticacion":"97156",
			"autenticacion":false
		}
	}
}
```



