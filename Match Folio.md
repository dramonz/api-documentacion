﻿# Match Folio

## Descripción

El microservicio match folio indica si una persona existe o no en la base de datos de circulo de credito y genera devuelve la clave de la persona y folio de la consulta.

## URL
> http://localhost:8081/mx/v1/personas/matchFolio

## HTTP Method: POST

### Request Parameters

|Propiedad|Tipo|¿Requerido?|Descripción|
|-------|-------|-------|-------|
|primerNombre|String|Si|El **primer nombre** completo del cliente, No usar abreviaturas, iniciales, acentos y/o puntos.
|segundoNombre|String|No|El **Segundo nombre** completo del cliente, No usar abreviaturas, iniciales, acentos y/o puntos.
|apellPaterno|String|Si|El **Apellido Paterno** completo del Cliente. No usar abreviaturas, iniciales y/o puntos.
|apellMaterno|String|No|El **Apellido Materno** completo del Cliente. No usar abreviaturas, iniciales y/o puntos.
|apellAdicional|String|No|Se debe utilizar para reportar el Apellido de casada.
|lsFecha|String|Si|Fecha de nacimiento
|rfc|String|No|Reglas:<br> Las primeras 4 posiciones deben ser alfabéticas. <br>5 y 6 deben contener un número entre 00 y 99. <br>7 y 8 deben contener un número entre 01 a 12. <br>9 y 10 deben contener un número entre 01 a 31. <br>11 -13 homoclave (opcional). <br>Los RFC's de personas extranjeras deben cumplir con las características arriba mencionadas
|direccion|String|Si|La dirección del cliente incluyendo nombre de la calle, número exterior y/o interior. Deben considerarse avenida, cerrada, manzana, lote, edificio, departamento etc.
|colonia|String|No|La colonia a la cual pertenece la dirección del cliente
|ciudad|String|No|La Ciudad a la cual pertenece la dirección del cliente
|codigo|String|Si|El código postal reportado debe estar compuesto por 5 dígitos; para que este sea válido deberá corresponder al rango que se maneja en dicho Estado. <br>En caso de una longitud de 4 dígitos completar con apóstrofe y cero a la izquierda ('08564). Ver Tabla: Rangos Códigos Postales
|delegMunic|String|No|La delegación a la pertenece la dirección del cliente
|estado|String|Si|La abreviatura correspondiente**.Ver en Tabla: Estados de la República
|pais|String|Si|Debe contener el país donde se encuentra el domicilio del Acreditado. Ver Anexos A: Tabla de Nacionalidades
|aplica|boolean|Si|Si el parametro viene true actualiza el expediente en caso de false no actualiza ni inserta solo busca
|curp|String|No|Reglas:<br>1- 4 posiciones deben ser alfabéticas. <br>2- 5 y 6 posiciones deben contener un número entre 00 y 99 (año). <br>3- 7 y 8 posiciones deben contener un número entre 01 y 12 (mes). <br>4- 9 y 10 posiciones deben contener un número entre 01 y 31 (día).<br>5- 11-16 posiciones deben ser alfabéticas. <br>6- 17 numérico (homoclave).<br>7- 18 numérico (Dígito Verificador).
|folio|String|Si|
|usuario|String|Si|Usuario que consulta el servicio


#### Ejemplo Request - JSON

```bash
curl -H "Content-Type: application/json" -X POST -d '{"primerNombre":"JUAsN","segundoNombre":"","apellPaterno":"PRUEBA","apellMaterno":"OCHO","apellAdicional":"","lsFecha":"1980-01-08","direccion":"INSURGENTES SUR 1008","colonia":"INSURGENTES","ciudad":"BENITO JUAREZ","codigo":"04470","delegMunic":"INSURGENTES","estado":"DF","rfc":"COPJ800108","pais":"MEX","aplica":false,"curp": "SASSASSASSA","folio":"API123","usuario":"CDCAPI0001"}' http://localhost:8081/mx/v1/personas/matchFolio
```

#### Response Properties
|Propiedad|Tipo|Descripción|
|-------|-------|-------|
|nperscve|int|Clave de la Persona
|hit|boolean|Resultado busqueda (encontrado  o no)
|tiempoBusqueda|int|Tiempo de busqueda ms
|nconencfolio|int|Id Transacción



#### Ejemplo Response - JSON

```bash
{
	"response":{
		"isSuccess":true,
		"success":{
			"title":"Petición exitosa",
			"message":"Se conecto de forma correcta"
		},
		"errors":[],
		"data":{
			"nperscve":40212897,
			"hit":true,
			"tiempoBusqueda":105,
			"nconencfolio":18582470
		}
	}
}
```

