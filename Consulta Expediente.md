﻿# Consulta Expediente

## Descripción

Consulta el expediente de una persona y genera un folio de consulta mediante la informacion de la persona, asi obteniendo toda la informacion crediticia de la persona si esta existe.

El servicio cuenta con dos opciones de solitud una mediante una simple petición **Rest** y otra mediante una consulta **[Graphql](http://graphql.org/learn/)**.

## Solicitud simple rest

Se realiza la consulta mediante una solicitud simple **Rest**.

### URL
> http://localhost:8080/mx/v1/personas/expediente

### HTTP Method: POST

#### Request Parameters

|Propiedad|Tipo|¿Requerido?|Descripción|
|-------|-------|-------|-------|
|primerNombre|String|Si|El **primer nombre** completo del cliente, No usar abreviaturas, iniciales, acentos y/o puntos.
|segundoNombre|String|No|El **segundo nombre** completo del cliente, No usar abreviaturas, iniciales, acentos y/o puntos.
|apellPaterno|String|Si|El **apellido paterno** completo del Cliente. No usar abreviaturas, iniciales y/o puntos.
|apellMaterno|String|No|El **apellido materno** completo del Cliente. No usar abreviaturas, iniciales y/o puntos.
|apellAdicional|String|No|Se debe utilizar para reportar el **apellido de casada**.
|lsFecha|String|Si|**Fecha de nacimiento**
|rfc|String|No|Reglas:<br>Las primeras 4 posiciones deben ser alfabéticas.<br>1. 5 y 6 deben contener un número entre 00 y 99.<br>2. 7 y 8 deben contener un número entre 01 a 12.<br>3. 9 y 10 deben contener un número entre 01 a 31.  <br>4. 11 -13 homoclave (opcional).<br> Los RFC's de personas extranjeras deben cumplir con las características arriba mencionadas|
|direccion|String|Si|La **dirección de la persona** incluyendo nombre de la calle, número exterior y/o interior. Deben considerarse avenida, cerrada, manzana, lote, edificio, departamento etc.
|colonia|String|No|La **colonia** a la cual pertenece la dirección de la persona
|ciudad|String|No|La **Ciudad** a la cual pertenece la dirección de la persona
|codigo|String|Si|El **código postal** reportado debe estar compuesto por 5 dígitos; para que este sea válido deberá corresponder al rango que se maneja en dicho Estado.  <br>En caso de una longitud de 4 dígitos completar con apóstrofe y cero a la izquierda ('08564)
|delegMunic|String|No|La **delegación** a la pertenece la dirección de la persona
|estado|String|Si|La abreviatura correspondiente
|pais|String|Si|Debe contener el país donde se encuentra el domicilio del Acreditado
|curp|String|No|Reglas: <br>  1- 4 posiciones deben ser alfabéticas.  <br>2- 5 y 6 posiciones deben contener un número entre 00 y 99 (año).<br>3- 7 y 8 posiciones deben contener un número entre 01 y 12 (mes).<br>4- 9 y 10 posiciones deben contener un número entre 01 y 31 (día).  <br>5- 11-16 posiciones deben ser alfabéticas.  <br>6- 17 numérico (homoclave).  <br>7- 18 numérico (Dígito Verificador).
|folio|String|Si|
|usuario|String|Si|Usuario que consulta el servicio

#### Ejemplo Request - JSON

```bash
curl -H "Content-Type: application/json" -X POST -d '{"primerNombre":"JUAN","segundoNombre":"","apellPaterno":"PRUEBA","apellMaterno":"OCHO","apellAdicional":"","lsFecha":"1980-01-08","direccion":"INSURGENTES SUR 1008","colonia":"INSURGENTES","ciudad":"BENITO JUAREZ","codigo":"04470","delegMunic":"INSURGENTES","estado":"DF","rfc":"COPJ800108","pais":"MEX","curp": "SASSASSASSA","folio":"API123","usuario":"CDCAPI0001"}' http://localhost:8080/mx/v1/personas/expediente
```

#### Response Properties
|Propiedad|Tipo|Descripción|
|-------|-------|-------|
|**claveFolio**|PeticionClaveFolio|Clave de la Persona
|└─clavePersona|int|Clave de la persona
|└─folioConsulta|int|Folio de consulta
|**consultas** |Lista Consulta |Lista de consulta
|└─fechaRegistro|String|Fecha en que Círculo de Crédito registró la consulta. Formato de fecha AAA-MM-DD.
|└─nombreOtorgante|String|Se presentará el Nombre de la Institución Otorgante que reporta la cuenta, el dato es asignado por Círculo de Crédito.
|└─direccionOtorgante|String|Dirección de otorgante
|└─nNumTelOtorgante|String|Número telefonico de Otorgante
|└─cveContrato|String|Tipo de prestamo que se solicito
|└─claveUnidadMonetaria|String|Clave de moneda
|└─importeContrato|String|Monto solicitado
|└─cveTipoResp|String|Tipo de responsabilidad respecto al crédito solicitado
|**creditos** |List Credito |Lista de creditos
|└─fechaActualizacion|Date|Fecha (AAAAMMDD) correspondiente al periodo que se está reportando.
|└─regImpugnado|int|Si el crédito fue impugnado (tiene alguna investigación por alguna controversia ya sea por parte del usuario o el cliente)
|└─claveOtorgante|String|Reportar la. Clave asignada por Círculo de Crédito a 10 posiciones.
|└─nombreOtorgante|String|Reportar el nombre del Otorgante que reporta la cuenta. Cuando se trate de un reporte especial o sea una cuenta propia de la institución al momento de la consulta, este nombre aparecerá en la sección de detalle de créditos en el reporte de Crédito. El dato correspondiente a este elemento es asignado por Círculo de Crédito.
|└─noCuenta|String|Es el número de cuenta del crédito, este número es asignado por el Usuario.<br>Todas las cuentas reportadas en una base deben ser únicas por contrato.
|└─cveTipoResp|String|Indica la responsabilidad que tiene el Cliente con el crédito otorgado.
|└─tipoCta|String|Indica el tipo de cuenta que el Usuarios dio al Cliente. Ver Tabla: Tipo de Cuenta
|└─cveContrato|String|El producto que se otorgó al Cliente.
|└─claveUnidadMonetaria|String|Los valores posibles son los siguientes:<br>MX = Pesos <br>US = Dólares <br>UD = Unidades de inversión
|└─valorActivo|Long|Aplica en pagos fijos (F) o hipotecario (H), el dato se refiere al valor total del activo para propósitos de evaluación o recuperación. Es el valor monetario de la garantía.
|└─numPagos|Long|Es el total de pagos determinado en la apertura del crédito. Si el tipo de cuenta es Pagos fijos (F) o Hipoteca (H), este elemento se hace Requerido con un valor mayor a cero, para otro Tipo de Cuenta es opcional
|└─frecPagos|String|Reportar la perioricidad con que el Cliente debe realizar sus pagos. Ver Tabla: Frecuencia de Pagos
|└─montoPagar|Long|Es la cantidad que el Cliente debe pagar de acuerdo con la frecuencia de pagos establecida. La cantidad a pagar debe ser un número entero mayor o igual a cero. (Monto acordado de cada parcialidad). Ver Tabla: Monto a Pagar y Saldo Actual
|└─fechaApertura|Date|Reportar la Fecha en la que se otorgó el crédito. <br>La fecha no debe ser mayor a 100 años
|└─fechaUltimoPago|Date|Es la fecha más reciente en la que el Cliente efectuó un pago. Ver Tabla: Fecha de Último Pago y Fecha de Última compra
|└─fechaUltimaCompra|Date|Fecha más reciente en que el Clientes efectuó una compra o disposición de un crédito. Si el tipo de cuenta es Fijo o Hipotecario, la Fecha de Última compra será igual a la Fecha de Apertura de la Cuenta. Ver Tabla: Fecha de Último Pago y Fecha de Última compra
|└─fechaCierreCta|Date|Fecha en la que se liquidó o cerró un crédito.
|└─fechaRep|Date|
|└─fechaUltCeros|Date|La ultima vez que la tarjeta de credito quedo en ceros
|└─garantia|String|Reportar una descripción de la garantía utilizada para asegurar el crédito otorgado.
|└─creditoMaximo|Long|Contiene el máximo importe de crédito utilizado por el Cliente. El valor debe ser mayor o igual a cero. Aplicable a Tipo de cuenta (F) Pagos Fijos, (H) Hipoteca y (R) Revolvente Ver Tabla: Crédito Máximo.
|└─saldoActual|Long|La cantidad debe ser un número entero. Es el importe total del adeudo que tiene el Clientes a la fecha de reporte incluyendo intereses. En caso de no tener saldo este elemento debe reportarse en cero (0) para cerrar la cuenta.
|└─limiteCredito|Long|El límite de crédito que el Usuario extiende al Cliente. <br>Este campo es obligatorio para créditos revolventes.
|└─saldoVencido|Long|Cantidad generada a la fecha de reporte por atraso en pagos. Deberá ser un número entero y positivo. Ver Tabla: Saldo Actual y Saldo Vencido
|└─sMOPXML|String|
|└─cveOrigen|String|
|└─sHistoricoXML|String|únicamente en el primer envió de la cuenta a Círculo de Crédito
|└─formaPagos|String|Forma de pagos
|└─formaPagoHist|String|
|└─sHPXml|String|
|└─fechaRecHistPago|Date|
|└─fechaAntiguaHistPago|Date|
|└─cveobservacion|String|Se utiliza para reportar situaciones especiales que presenta la cuenta (por ejemplo para deducir impuestos de un prestamo que el banco considera que ya no se pagará
|└─totalPagoRep|Long|Es el total de pagos realizados por el Cliente a la fecha que se está reportando. Es númerico de 3
|└─peorAtraso|int|La mayor cantidad de pagos incumplidos en el historico del cliente
|└─fechaPeorAtraso|Date|La Fecha (AAAAMMDD) en la que el cliente tuvo su peor atraso
|└─saldoVencPeorAtraso|Long|Saldo vencido del peor atraso
|└─tipoNegocio|String|tipo de negocio
|└─sIdCuenta|String|
|**domicilios**|Lista Domicilio|Lista de domicilios
|└─tipoConsulta|Integer|Fecha en que se fue consultado el historial
|└─importeSolicitud|Integer|Importe de la solicitud
|└─numDependientes|Integer|numero dependientes
|└─idioma|Integer|idioma 
|└─hit|Integer|
|└─tipoDomicilio|String|Tipo Domicilio
|└─colonia|String|La colonia a la cual pertenece la dirección de la persona
|└─delegMunic|String|La delegación a la pertenece la dirección de la persona
|└─ciudad|String|La Ciudad a la cual pertenece la dirección de la persona
|└─estado|String|La abreviatura correspondiente**.Ver en Tabla: Estados de la República
|└─codigo|String|El código postal reportado debe estar compuesto por 5 dígitos; para que este sea válido deberá corresponder al rango que se maneja en dicho Estado.<br> En caso de una longitud de 4 dígitos completar con apóstrofe y cero a la izquierda ('08564).
|└─fechaResidencia|Date|Fecha de la residencia
|└─tipoRes|Integer|Tipo de Residencia
|└─folio|String|Folio de la operacion
|└─tipoAsent|String|Tipo de Asentamiento
|└─direccion|String|Direccion
|└─numExterior|Integer|numero exterior de la vivienda
|└─numInterior|Integer|numero interior de la vivienda
|└─usuario|String|Usuario
|└─fechaRegistro|Date|Fecha de Registro
|└─loteCarga|Integer|Lote de carga
|└─nPais|Integer|codigo de Pais
|└─cveRes|Integer|Clave de residencia
|└─datosDireccion|Boolean|
|└─datosEmpleo|Boolean|datos de el empleado
|└─refOperador|String|
|└─cIdPaisEmp|String|
|└─nIdMun|Integer|
|└─nIdEdo|Integer|
|└─fechaProcCteDom|Date|
|└─prodRequerido|Integer|
|└─clavePersona|Integer|Clave de persona
|└─tipoPersonaRelacion|Integer|
|└─cvePersonaRel|Integer|
|└─numExperiencias|Integer|
|└─valorUDI|Integer|
|empleos|List<Empleo>|Lista de empleos
|└─folio|String|
|└─cvepersona|int|Clave de la persona
|└─generico|int|
|└─nombre|String|Debe reportarse el Nombre o Razón Social de la empresa que emplea al Cliente. <br>Cuando el consumidor sea trabajador independiente, no está asociado a una empresa o no cuenta con un trabajo podrá reportarse uno de los siguientes posibles valores:<br>•Trabajador Independiente <br>•Estudiante<br>•Labores de Hogar<br>•Jubilado<br>•Desempleado<br>•Exempleado"
|└─direccion|String|La dirección del cliente incluyendo nombre de la calle, número exterior y/o interior. Deben considerarse avenida, cerrada, manzana, lote, edificio, departamento etc.
|└─colonia|String|La colonia a la cual pertenece la dirección del cliente
|└─delegMunic|String|La delegación a la pertenece la dirección del cliente
|└─ciudad|String|La Ciudad a la cual pertenece la dirección del cliente
|└─estado|String|La abreviatura correspondiente**.Ver en Tabla: Estados de la República
|└─codigo|String|El código postal reportado debe estar compuesto por 5 dígitos; para que este sea válido deberá corresponder al rango que se maneja en dicho Estado. <br>En caso de una longitud de 4 dígitos completar con apóstrofe y cero a la izquierda ('08564). Ver Tabla: Rangos Códigos Postales"
|└─numTelefono|String|"Reportar el número telefónico de empleo del Acreditado.<br>Cada caracter debe ser un número de 0-9, si se ingresa cualquier otro caracter el registro será rechazado.<br>En caso de no contar con algun empleo, colocar el mismo dato del teléfono del acreditado."
|└─numExtension|String|Si se cuenta con la información reportar la extensión telefónica de la persona.
|└─numFax|String|Mismos criterios de validación que en el Elemento Número Telefónico
|└─puesto|String|Reportar el título o posición de empleo del Acreditado, si se tiene disponible
|└─fechaAlta|Date|
|└─unidadMonetaria|String|Es el tipo de moneda que se le paga al Acreditado en su empleo. <br>Valores para este Elemento: <br>MX = Pesos Mexicanos <br>US = Dólares <br>UD = Unidades de Inversión. <br>En caso de Reportar Salario Mensual este elemento se hace requerido.
|└─salarioMensual|String|Reportar el Ingreso Mensual del Acreditado. <br>En caso de colocar Clave de Moneda este elemento es requerido. <br>Se aceptan números enteros y sin caracteres especiales.
|└─fechaUltDia|Date|Debe reportarse la fecha del último día de trabajo en esa empresa.
|└─fechaVerif|Date|Fecha en que el otorgante verificó los datos proporcionados por el Acreditado
|└─usuario|String|
|**persona** |Persona |Persona
|└─primerNombre|String|El primer nombre completo del cliente, No usar abreviaturas, iniciales, acentos y/o puntos.
|└─segundoNombre|String|El Segundo nombre completo del cliente, No usar abreviaturas, iniciales, acentos y/o puntos.
|└─apellPaterno|String|El Apellido Paterno completo del Cliente. No usar abreviaturas, iniciales y/o puntos.
|└─apellMaterno|String|El Apellido Materno completo del Cliente. No usar abreviaturas, iniciales y/o puntos.
|└─fechaNac|Date|Fecha de Nacimiento
|└─sexo|String|Sexo
|└─rfc|String|Reglas: <br>Las primeras 4 posiciones deben ser alfabéticas. <br>5 y 6 deben contener un número entre 00 y 99. <br>7 y 8 deben contener un número entre 01 a 12. <br>9 y 10 deben contener un número entre 01 a 31. <br>11 -13 homoclave (opcional). <br>Los RFC's de personas extranjeras deben cumplir con las características arriba mencionadas
|└─importeSolicitud|Integer|Importe Solicitud
|└─numDependientes|Integer|El número de dependientes económicos del Cliente
|└─edadDependientes|String|La edad de los dependientes económicos del Cliente
|└─idioma|Integer|Idioma
|└─fechaAlta|Date|Fecha de Alta
|└─hit|Integer|
|└─prefijo|String|Prefijo
|└─sufijo|String|Sufijo
|└─regAdicional|String|Registro Adicional
|└─pais|String|Debe contener el país donde se encuentra el domicilio del Acreditado. Ver Anexos A: Tabla de Nacionalidades
|└─tipoRes|Integer|
|└─edoCivil|String|Estado Civil
|└─folio|String|
|└─curp|String|Reglas: <br>1- 4 posiciones deben ser alfabéticas. <br>2- 5 y 6 posiciones deben contener un número entre 00 y 99 (año). <br>3- 7 y 8 posiciones deben contener un número entre 01 y 12 (mes). <br>4- 9 y 10 posiciones deben contener un número entre 01 y 31 (día).<br>5- 11-16 posiciones deben ser alfabéticas. <br>6- 17 numérico (homoclave).<br>7- 18 numérico (Dígito Verificador).
|└─nombre|String|Nombre de la persona
|└─numExterior|Integer|Número exterior
|└─numInterior|Integer|Número Interior
|└─usuario|String|
|└─tipoPersona|String|Tipo de persona
|└─fechaRegistro|Date|Fecha de Registro
|└─fechaDefuncion|Date|Fecha de Defuncion
|└─email|String|Correo electronico
|└─obs|String|
|└─fechaProcCte|Date|
|└─fechaActual|Date|Fecha Actual
|└─loteCarga|Integer|
|└─nPais|Integer|
|└─cveRes|Integer|
|└─datosDireccion|Boolean|
|└─datosEmpleo|Boolean|
|└─refOperador|String|
|└─nIdMun|Integer|
|└─nIdEdo|Integer|
|└─tipoProducto|Integer|
|└─prodRequerido|Integer|
|└─clavePersona|String|
|└─tipoPersonaRelacion|String|
|└─cvePersonaRel|Integer|
|└─numExperiencias|Integer|
|└─valorUDI|Integer|


#### Ejemplo Response - JSON

```bash

{
	"response":{
		"isSuccess":true,
		"success":{
			"title":"Petición exitosa",
			"message":"Se conecto de forma correcta"
		},
		"errors":[],
		"data":{
			"claveFolio":{
				"clavePersona":40212897,
				"folioConsulta":18582443
			},
			"consultas":[
				{
					"fechaRegistro":"2018-04-09",
					"nombreOtorgante":"CONSUPRESTA",
					"direccionOtorgante":"PASEO DE LA REFORMA",
					"cveContrato":"F",
					"claveUnidadMonetaria":"MX",
					"importeContrato":"1",
					"cveTipoResp":"F"
				},
				{
					"fechaRegistro":"2018-04-06",
					"nombreOtorgante":"CONSUPRESTA",
					"direccionOtorgante":"PASEO DE LA REFORMA",
					"cveContrato":"F",
					"claveUnidadMonetaria":"MX",
					"importeContrato":"1",
					"cveTipoResp":"F"
				}
			],
			"creditos":[  
				{
	                "fechaActualizacion": 1340582400000,
	                "regImpugnado": 1,
	                "claveOtorgante": "000015",
	                "nombreOtorgante": "REPORTE DE CREDITO ESPECIAL",
	                "noCuenta": "000917680000018121",
	                "cveTipoResp": "I",
	                "tipoCta": "P",
	                "cveContrato": "PP",
	                "claveUnidadMonetaria": "MX",
	                "valorActivo": 5960,
	                "numPagos": 70,
	                "frecPagos": "S",
	                "montoPagar": 0,
	                "fechaApertura": 1337558400000,
	                "fechaUltimoPago": 1340582400000,
	                "fechaUltimaCompra": 1337558400000,
	                "fechaCierreCta": 1340582400000,
	                "fechaRep": 1340582400000,
	                "fechaUltCeros": -62135769600000,
	                "creditoMaximo": 6198,
	                "saldoActual": 0,
	                "limiteCredito": 0,
	                "saldoVencido": 0,
	                "cveOrigen": "1",
	                "formaPagos": " V",
	                "formaPagoHist": "",
	                "totalPagoRep": 0,
	                "peorAtraso": 0,
	                "tipoNegocio": "0024"
	            }
            ],	
			"domicilios":[
				{
					"tipoConsulta":0,
					"importeSolicitud":0,
					"numDependientes":0,
					"idioma":0,
					"hit":0,
					"tipoDomicilio":null,
					"colonia":null,
					"delegMunic":null,
					"ciudad":"MEXICO",
					"estado":"DF",
					"codigo":"11230",
					"fechaResidencia":1521158400000,
					"tipoRes":0,
					"folio":"18582443",
					"tipoAsent":"0",
					"direccion":"INSURGENTES SUR 1007",
					"numExterior":0,
					"numInterior":0,
					"usuario":"0",
					"fechaRegistro":1521158400000,
					"loteCarga":0,
					"nPais":0,
					"cveRes":0,
					"aplica":false,
					"datosDireccion":true,
					"datosEmpleo":false,
					"refOperador":"                         ",
					"cIdPaisEmp":"MX",
					"nIdMun":0,
					"nIdEdo":0,
					"fechaProcCteDom":1521158400000,
					"tipoProducto":0,
					"estatusAutent":false,
					"autenticado":false,
					"prodRequerido":0,
					"clavePersona":0,
					"tipoPersonaRelacion":0,
					"cvePersonaRel":0,
					"numExperiencias":0,
					"valorUDI":0
				}
			],
			"empleos":[  {
                    "folio": "18580501",
                    "cvepersona": 0,
                    "generico": 0,
                    "nombre": null,
                    "direccion": null,
                    "colonia": null,
                    "delegMunic": null,
                    "ciudad": null,
                    "estado": null,
                    "codigo": "92100",
                    "numTelefono": null,
                    "numExtension": null,
                    "numFax": null,
                    "puesto": "MANTENIMIENTO",
                    "fechaAlta": -2177452800000,
                    "unidadMonetaria": null,
                    "salarioMensual": null,
                    "fechaUltDia": 1247616000000,
                    "fechaVerif": 1247616000000,
                    "usuario": "0"
                }],
			"persona":{
					"primerNombre":"JUAN ",
					"apellPaterno":"PRUEBA",
					"apellMaterno":"OCHO",
					"fechaNac":316137600000,
					"rfc":"COPJ800108",
					"importeSolicitud":0,
					"numDependientes":0,
					"idioma":0,
					"fechaAlta":1507852800000,
					"hit":0,
					"pais":"MX",
					"tipoRes":0,
					"folio":"18582443",
					"nombre":"JUAN  PRUEBA OCHO ",
					"numExterior":0,
					"numInterior":0,
					"usuario":"0",
					"tipoPersona":"PF",
					"fechaRegistro":1507852800000,
					"fechaDefuncion":-2177452800000,
					"fechaProcCte":1507852800000,
					"fechaActual":1507852800000,
					"loteCarga":0,
					"nPais":0,
					"cveRes":5,
					"aplica":false,
					"datosDireccion":false,
					"datosEmpleo":false,
					"refOperador":"                         ",
					"nIdMun":0,
					"nIdEdo":0,
					"tipoProducto":0,
					"estatusAutent":false,
					"autenticado":false,
					"prodRequerido":0,
					"clavePersona":"0",
					"tipoPersonaRelacion":"0",
					"cvePersonaRel":0,
					"numExperiencias":0,
					"valorUDI":0
				}
		}
	}
}
```


##  Solicitud mediante Graphql

La solicitud via graphql se realiza mediante la sintaxis de [graphql](http://graphql.org/learn/)  con los mismos parametros de entrada que en la consuta normal, la salida depende de los filtro de la consutal generada.

### URL
> http://localhost:8080/mx/v1/personas/expediente/graphql

### HTTP Method: POST

#### Ejemplo Request - GRAPHQL

```bash
curl -H "Content-Type: application/graphql" -X POST -d '{  expediente(primerNombre:"JUAN",segundoNombre:"",apellPaterno:"PRUEBA",apellMaterno:"OCHO",apellAdicional:"",lsFecha:"1980-01-08",direccion:"INSURGENTES SUR 1008",colonia:"INSURGENTES",ciudad:"BENITO JUAREZ",codigo:"04470",delegMunic:"INSURGENTES",estado:"DF",rfc:"COPJ800108",pais:"MEX",curp: "SASSASSASSA",folio:"API123",usuario:"CDCAPI0001") { 
clavePersona 
folioConsulta
empleos {
	puesto
	folio 
}
domicilios {
	colonia
	ciudad
}
consultas { 
	fechaRegistro
	nombreOtorgante
} 
creditos {
	nombreOtorgante
	noCuenta
	creditoMaximo
}
}
}
' http://localhost:8080/mx/v1/personas/expediente/graphql
```

#### Ejemplo Response - JSON

```bash
{
	"response":{
		"isSuccess":true,
		"success":{
			"title":"Petición exitosa",
			"message":"Se conecto de forma correcta"
		},
		"errors":[],
		"data":{	
			"expediente":{
				"clavePersona":40212897,
				"folioConsulta":18582452,
				"empleos":[],
				"domicilios":[
					{
						"colonia":null,
						"ciudad":"MEXICO"
					},
					{
						"colonia":"insurgentes",
						"ciudad":"BENITO JUAREZ"
					}
				],
				"consultas":[
					{
						"fechaRegistro":"2018-04-09",
						"nombreOtorgante":"CONSUPRESTA"
					},
					{
						"fechaRegistro":"2018-04-06",
						"nombreOtorgante":"CONSUPRESTA"
					},					
					{
						"fechaRegistro":"2018-03-14",
						"nombreOtorgante":"CONSUPRESTA"
					}
				],
				"creditos":[]
			}
		}
	}
}
```


