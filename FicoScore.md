﻿# Microservicio de FicoScore

## Descripción

Obtiene el fico score de una persona mediante las clave de la persona,el folio de la consulta, tipo de score y la clave del otorgante

## URL
> http://localhost:8080/mx/v1/personas/{cvePersona}/fico?folio={folio}&tipoScore=1&cveOtorgante={cveOtorgante}

### HTTP Method: POST

#### Request Parameters

|Propiedad|Tipo|¿Requerido?|Descripción|
|-------|-------|-------|-------|
|cvePersona|int|Si|Clave de la Persona
|folio|int|Si|Folio de consulta
|tipoScore|int|Si|Tipo de Score
|cveOtorgante|String|Si|Clave de otorgante


#### Ejemplo Request - JSON

```bash
url -H "Content-Type: application/json" -X GET "http://localhost:8080/mx/v1/personas/40112129/fico?folio=18571792&tipoScore=1&cveOtorgante=000214D"

```

#### Response Properties
|Propiedad|Tipo|Descripción|
|-------|-------|-------|
|score|int|Resultado de la evaluacion del expediente
|reason1|String|Codigo de Razon 1 Asociado al Score
|descReason1|String|Descrpcion de Razon 1 Asociado al Score
|reason2|String|Codigo de Razon 2 Asociado al Score
|descReason2|String|Descrpcion de Razon 2 Asociado al Score
|reason3|String|Codigo de Razon 3 Asociado al Score
|descReason3|String|Descrpcion de Razon 3 Asociado al Score
|reason4|String|Codigo de Razon 4 Asociado al Score
|descReason4|String|Descripcion de Razon 4 Asociado al Score.
|exclusionCode|int|Codigo de Exclusion
|erroCode|int|Codigo de error


#### Ejemplo Response - JSON

```bash
{
	"response":
	{
		"isSuccess":true,
		"success":
		{
			"title":"Petición exitosa",
			"message":"Se conecto de forma correcta"
		},
		"errors":[],
		"data":
			{
				"score":713,
				"reason1":"R7",
				"descReason1":"Muy pocas cuentas a plazo fijo.",
				"reason2":"E0",
				"descReason2":"Información Demográfica.",
				"reason3":"E2",
				"descReason3":"Número de consultas.",
				"reason4":"G1",
				"descReason4":"Falta de información reciente de cuentas revolventes.",
				"exclusionCode":0,
				"erroCode":0
			}
	}
}
```


